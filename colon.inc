%define node 0
%macro colon 2
    %2:
        dq node
        db %1, 0
        %define node %2
%endmacro