
section .text

%define pointer_size 8

%include "lib.inc"
global find_word

find_word:
        push    r14
    .loop:
        mov r14, rsi
        add rsi, pointer_size
        call string_equals

        test rax, rax
        jz .not_found
        mov rax, r14
        
        jmp .err
    .not_found:
        mov rsi, r14
        mov rsi, [rsi]
        
        test rsi, rsi
        jnz .loop
        xor rax, rax
    .err:
        pop r14
        ret